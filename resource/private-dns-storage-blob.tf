###############################################################################
# Optional Variables
###############################################################################

variable "private_dns_storage_blob_name" {
  type        = string
  default     = "privatelink.blob.core.windows.net"
  description = "The private DNS zone name for storage account blob container private link connections."
}

variable "private_dns_storage_blob_enabled" {
  type        = bool
  default     = false
  description = "Should a private DNS zone be created for storage account blob container private link connections."
}

###############################################################################
# Resources
###############################################################################

resource "azurerm_private_dns_zone" "storage_blob" {
  count               = var.private_dns_storage_blob_enabled ? 1 : 0
  name                = var.private_dns_storage_blob_name
  resource_group_name = azurerm_resource_group.object.name

  tags = {
    "Name"         = var.private_dns_storage_blob_name
    "Group"        = azurerm_resource_group.object.name
    "Owner"        = var.owner
    "Company"      = var.company
    "Location"     = var.location
    "Subscription" = data.azurerm_subscription.current.display_name
  }
}

###############################################################################
# Outputs
###############################################################################

output "private_dns_storage_blob_enabled" {
  value = var.private_dns_storage_blob_enabled
}

###############################################################################

output "private_dns_storage_blob_id" {
  value = length(azurerm_private_dns_zone.storage_blob) == 1 ? azurerm_private_dns_zone.storage_blob[0].id : null
}

output "private_dns_storage_blob_name" {
  value = length(azurerm_private_dns_zone.storage_blob) == 1 ? azurerm_private_dns_zone.storage_blob[0].name : null
}

output "private_dns_storage_blob_record_max" {
  value = length(azurerm_private_dns_zone.storage_blob) == 1 ? azurerm_private_dns_zone.storage_blob[0].max_number_of_record_sets : null
}

output "private_dns_storage_blob_record_count" {
  value = length(azurerm_private_dns_zone.storage_blob) == 1 ? azurerm_private_dns_zone.storage_blob[0].number_of_record_sets : null
}

output "private_dns_storage_blob_vnet_link_max" {
  value = length(azurerm_private_dns_zone.storage_blob) == 1 ? azurerm_private_dns_zone.storage_blob[0].max_number_of_virtual_network_links : null
}

output "private_dns_storage_blob_vnet_registration_link_max" {
  value = length(azurerm_private_dns_zone.storage_blob) == 1 ? azurerm_private_dns_zone.storage_blob[0].max_number_of_virtual_network_links_with_registration : null
}

###############################################################################
