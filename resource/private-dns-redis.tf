###############################################################################
# Optional Variables
###############################################################################

variable "private_dns_redis_name" {
  type        = string
  default     = "privatelink.redis.cache.windows.net"
  description = "The private DNS zone name for Redis private link connections."
}

variable "private_dns_redis_enabled" {
  type        = bool
  default     = false
  description = "Should a private DNS zone be created for Redis private link connections."
}

###############################################################################
# Resources
###############################################################################

resource "azurerm_private_dns_zone" "redis" {
  count               = var.private_dns_redis_enabled ? 1 : 0
  name                = var.private_dns_redis_name
  resource_group_name = azurerm_resource_group.object.name

  tags = {
    "Name"         = var.private_dns_redis_name
    "Group"        = azurerm_resource_group.object.name
    "Owner"        = var.owner
    "Company"      = var.company
    "Location"     = var.location
    "Subscription" = data.azurerm_subscription.current.display_name
  }
}

###############################################################################
# Outputs
###############################################################################

output "private_dns_redis_enabled" {
  value = var.private_dns_redis_enabled
}

###############################################################################

output "private_dns_redis_id" {
  value = length(azurerm_private_dns_zone.redis) == 1 ? azurerm_private_dns_zone.redis[0].id : null
}

output "private_dns_redis_name" {
  value = length(azurerm_private_dns_zone.redis) == 1 ? azurerm_private_dns_zone.redis[0].name : null
}

output "private_dns_redis_record_max" {
  value = length(azurerm_private_dns_zone.redis) == 1 ? azurerm_private_dns_zone.redis[0].max_number_of_record_sets : null
}

output "private_dns_redis_record_count" {
  value = length(azurerm_private_dns_zone.redis) == 1 ? azurerm_private_dns_zone.redis[0].number_of_record_sets : null
}

output "private_dns_redis_vnet_link_max" {
  value = length(azurerm_private_dns_zone.redis) == 1 ? azurerm_private_dns_zone.redis[0].max_number_of_virtual_network_links : null
}

output "private_dns_redis_vnet_registration_link_max" {
  value = length(azurerm_private_dns_zone.redis) == 1 ? azurerm_private_dns_zone.redis[0].max_number_of_virtual_network_links_with_registration : null
}

###############################################################################
