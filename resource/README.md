<!---------------------------------------------------------------------------->

# resource

#### Manage resource groups

-------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/group/azure//resource`**

-------------------------------------------------------------------------------

### Example Usage

```
module "my_resource_group" {
  source   = "gitlab.com/bitservices/group/azure//resource"
  name     = "foobar"
  owner    = "terraform@bitservices.io"
  company  = "BITServices Ltd"
  location = "uksouth"
}
```

<!---------------------------------------------------------------------------->
