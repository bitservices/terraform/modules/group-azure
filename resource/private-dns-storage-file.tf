###############################################################################
# Optional Variables
###############################################################################

variable "private_dns_storage_file_name" {
  type        = string
  default     = "privatelink.file.core.windows.net"
  description = "The private DNS zone name for storage account file share private link connections."
}

variable "private_dns_storage_file_enabled" {
  type        = bool
  default     = false
  description = "Should a private DNS zone be created for storage account file share private link connections."
}

###############################################################################
# Resources
###############################################################################

resource "azurerm_private_dns_zone" "storage_file" {
  count               = var.private_dns_storage_file_enabled ? 1 : 0
  name                = var.private_dns_storage_file_name
  resource_group_name = azurerm_resource_group.object.name

  tags = {
    "Name"         = var.private_dns_storage_file_name
    "Group"        = azurerm_resource_group.object.name
    "Owner"        = var.owner
    "Company"      = var.company
    "Location"     = var.location
    "Subscription" = data.azurerm_subscription.current.display_name
  }
}

###############################################################################
# Outputs
###############################################################################

output "private_dns_storage_file_enabled" {
  value = var.private_dns_storage_file_enabled
}

###############################################################################

output "private_dns_storage_file_id" {
  value = length(azurerm_private_dns_zone.storage_file) == 1 ? azurerm_private_dns_zone.storage_file[0].id : null
}

output "private_dns_storage_file_name" {
  value = length(azurerm_private_dns_zone.storage_file) == 1 ? azurerm_private_dns_zone.storage_file[0].name : null
}

output "private_dns_storage_file_record_max" {
  value = length(azurerm_private_dns_zone.storage_file) == 1 ? azurerm_private_dns_zone.storage_file[0].max_number_of_record_sets : null
}

output "private_dns_storage_file_record_count" {
  value = length(azurerm_private_dns_zone.storage_file) == 1 ? azurerm_private_dns_zone.storage_file[0].number_of_record_sets : null
}

output "private_dns_storage_file_vnet_link_max" {
  value = length(azurerm_private_dns_zone.storage_file) == 1 ? azurerm_private_dns_zone.storage_file[0].max_number_of_virtual_network_links : null
}

output "private_dns_storage_file_vnet_registration_link_max" {
  value = length(azurerm_private_dns_zone.storage_file) == 1 ? azurerm_private_dns_zone.storage_file[0].max_number_of_virtual_network_links_with_registration : null
}

###############################################################################
