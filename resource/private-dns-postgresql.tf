###############################################################################
# Optional Variables
###############################################################################

variable "private_dns_postgresql_suffix" {
  type        = string
  default     = "postgres.database.azure.com"
  description = "The private DNS zone suffix for Postgresql private link connections. The resource group name is appended to this."
}

variable "private_dns_postgresql_enabled" {
  type        = bool
  default     = false
  description = "Should a private DNS zone be created for Postgresql private link connections."
}

###############################################################################
# Locals
###############################################################################

locals {
  private_dns_postgresql_name = format("%s.%s", azurerm_resource_group.object.name, var.private_dns_postgresql_suffix)
}

###############################################################################
# Resources
###############################################################################

resource "azurerm_private_dns_zone" "postgresql" {
  count               = var.private_dns_postgresql_enabled ? 1 : 0
  name                = local.private_dns_postgresql_name
  resource_group_name = azurerm_resource_group.object.name

  tags = {
    "Name"         = local.private_dns_postgresql_name
    "Group"        = azurerm_resource_group.object.name
    "Owner"        = var.owner
    "Company"      = var.company
    "Location"     = var.location
    "Subscription" = data.azurerm_subscription.current.display_name
  }
}

###############################################################################
# Outputs
###############################################################################

output "private_dns_postgresql_suffix" {
  value = var.private_dns_postgresql_suffix
}

output "private_dns_postgresql_enabled" {
  value = var.private_dns_postgresql_enabled
}

###############################################################################

output "private_dns_postgresql_id" {
  value = length(azurerm_private_dns_zone.postgresql) == 1 ? azurerm_private_dns_zone.postgresql[0].id : null
}

output "private_dns_postgresql_name" {
  value = length(azurerm_private_dns_zone.postgresql) == 1 ? azurerm_private_dns_zone.postgresql[0].name : null
}

output "private_dns_postgresql_record_max" {
  value = length(azurerm_private_dns_zone.postgresql) == 1 ? azurerm_private_dns_zone.postgresql[0].max_number_of_record_sets : null
}

output "private_dns_postgresql_record_count" {
  value = length(azurerm_private_dns_zone.postgresql) == 1 ? azurerm_private_dns_zone.postgresql[0].number_of_record_sets : null
}

output "private_dns_postgresql_vnet_link_max" {
  value = length(azurerm_private_dns_zone.postgresql) == 1 ? azurerm_private_dns_zone.postgresql[0].max_number_of_virtual_network_links : null
}

output "private_dns_postgresql_vnet_registration_link_max" {
  value = length(azurerm_private_dns_zone.postgresql) == 1 ? azurerm_private_dns_zone.postgresql[0].max_number_of_virtual_network_links_with_registration : null
}

###############################################################################
