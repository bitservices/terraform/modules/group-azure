###############################################################################
# Optional Variables
###############################################################################

variable "private_dns_mariadb_name" {
  type        = string
  default     = "privatelink.mariadb.database.azure.com"
  description = "The private DNS zone name for MariaDB private link connections."
}

variable "private_dns_mariadb_enabled" {
  type        = bool
  default     = false
  description = "Should a private DNS zone be created for MariaDB private link connections."
}

###############################################################################
# Resources
###############################################################################

resource "azurerm_private_dns_zone" "mariadb" {
  count               = var.private_dns_mariadb_enabled ? 1 : 0
  name                = var.private_dns_mariadb_name
  resource_group_name = azurerm_resource_group.object.name

  tags = {
    "Name"         = var.private_dns_mariadb_name
    "Group"        = azurerm_resource_group.object.name
    "Owner"        = var.owner
    "Company"      = var.company
    "Location"     = var.location
    "Subscription" = data.azurerm_subscription.current.display_name
  }
}

###############################################################################
# Outputs
###############################################################################

output "private_dns_mariadb_enabled" {
  value = var.private_dns_mariadb_enabled
}

###############################################################################

output "private_dns_mariadb_id" {
  value = length(azurerm_private_dns_zone.mariadb) == 1 ? azurerm_private_dns_zone.mariadb[0].id : null
}

output "private_dns_mariadb_name" {
  value = length(azurerm_private_dns_zone.mariadb) == 1 ? azurerm_private_dns_zone.mariadb[0].name : null
}

output "private_dns_mariadb_record_max" {
  value = length(azurerm_private_dns_zone.mariadb) == 1 ? azurerm_private_dns_zone.mariadb[0].max_number_of_record_sets : null
}

output "private_dns_mariadb_record_count" {
  value = length(azurerm_private_dns_zone.mariadb) == 1 ? azurerm_private_dns_zone.mariadb[0].number_of_record_sets : null
}

output "private_dns_mariadb_vnet_link_max" {
  value = length(azurerm_private_dns_zone.mariadb) == 1 ? azurerm_private_dns_zone.mariadb[0].max_number_of_virtual_network_links : null
}

output "private_dns_mariadb_vnet_registration_link_max" {
  value = length(azurerm_private_dns_zone.mariadb) == 1 ? azurerm_private_dns_zone.mariadb[0].max_number_of_virtual_network_links_with_registration : null
}

###############################################################################
