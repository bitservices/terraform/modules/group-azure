###############################################################################
# Optional Variables
###############################################################################

variable "private_dns_mysql_suffix" {
  type        = string
  default     = "mysql.database.azure.com"
  description = "The private DNS zone suffix for MySQL private link connections. The resource group name is appended to this."
}

variable "private_dns_mysql_enabled" {
  type        = bool
  default     = false
  description = "Should a private DNS zone be created for MySQL private link connections."
}

###############################################################################
# Locals
###############################################################################

locals {
  private_dns_mysql_name = format("%s.%s", azurerm_resource_group.object.name, var.private_dns_mysql_suffix)
}

###############################################################################
# Resources
###############################################################################

resource "azurerm_private_dns_zone" "mysql" {
  count               = var.private_dns_mysql_enabled ? 1 : 0
  name                = local.private_dns_mysql_name
  resource_group_name = azurerm_resource_group.object.name

  tags = {
    "Name"         = local.private_dns_mysql_name
    "Group"        = azurerm_resource_group.object.name
    "Owner"        = var.owner
    "Company"      = var.company
    "Location"     = var.location
    "Subscription" = data.azurerm_subscription.current.display_name
  }
}

###############################################################################
# Outputs
###############################################################################

output "private_dns_mysql_suffix" {
  value = var.private_dns_mysql_suffix
}

output "private_dns_mysql_enabled" {
  value = var.private_dns_mysql_enabled
}

###############################################################################

output "private_dns_mysql_id" {
  value = length(azurerm_private_dns_zone.mysql) == 1 ? azurerm_private_dns_zone.mysql[0].id : null
}

output "private_dns_mysql_name" {
  value = length(azurerm_private_dns_zone.mysql) == 1 ? azurerm_private_dns_zone.mysql[0].name : null
}

output "private_dns_mysql_record_max" {
  value = length(azurerm_private_dns_zone.mysql) == 1 ? azurerm_private_dns_zone.mysql[0].max_number_of_record_sets : null
}

output "private_dns_mysql_record_count" {
  value = length(azurerm_private_dns_zone.mysql) == 1 ? azurerm_private_dns_zone.mysql[0].number_of_record_sets : null
}

output "private_dns_mysql_vnet_link_max" {
  value = length(azurerm_private_dns_zone.mysql) == 1 ? azurerm_private_dns_zone.mysql[0].max_number_of_virtual_network_links : null
}

output "private_dns_mysql_vnet_registration_link_max" {
  value = length(azurerm_private_dns_zone.mysql) == 1 ? azurerm_private_dns_zone.mysql[0].max_number_of_virtual_network_links_with_registration : null
}

###############################################################################
