###############################################################################
# Required Variables
###############################################################################

variable "name" {
  type        = string
  description = "The name for this resource group."
}

variable "owner" {
  type        = string
  description = "Owner of the resource."
}

variable "company" {
  type        = string
  description = "Company the resource belogs to."
}

variable "location" {
  type        = string
  description = "Datacentre location for this resource group."
}

###############################################################################
# Resources
###############################################################################

resource "azurerm_resource_group" "object" {
  name     = var.name
  location = var.location

  tags = {
    "Name"         = var.name
    "Owner"        = var.owner
    "Company"      = var.company
    "Location"     = var.location
    "Subscription" = data.azurerm_subscription.current.display_name
  }
}

###############################################################################
# Outputs
###############################################################################

output "owner" {
  value = var.owner
}

output "company" {
  value = var.company
}

###############################################################################

output "id" {
  value = azurerm_resource_group.object.id
}

output "name" {
  value = azurerm_resource_group.object.name
}

output "location" {
  value = azurerm_resource_group.object.location
}

###############################################################################
